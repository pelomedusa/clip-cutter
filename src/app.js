const { app } = require('electron/main')
const {createMainWindow} = require("./Windows/Main");
const {downloadFFmpeg} = require("./Windows/FFmpegDownload");

app.whenReady().then(() => {
    downloadFFmpeg().then(() => {
        createMainWindow()
    });
})
