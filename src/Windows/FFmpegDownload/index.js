const {BrowserWindow, remote} = require('electron/main')
const path = require('node:path')
const fs = require("fs");
const os = require('os');
const { finished } = require('stream/promises');
const { Readable } = require('stream');
const decompress = require('decompress');
const {exec} = require("child_process");

const APP_PATH = require("path").dirname(require('electron/main').app.getPath("exe"))
const WIN_FFMPEG_VERSION = '6.1.1';
const WIN_BINARIES_PATH = path.join(APP_PATH, `ffmpeg-${WIN_FFMPEG_VERSION}`)

let win;
const downloadFFmpeg = () => {
    return (process.platform === 'linux') ? handleLinux() : handleWindows();
}

const getFFmpegPath = () => {
  return (process.platform === 'linux') ?
            'ffmpeg'
            :
            path.join(WIN_BINARIES_PATH, 'ffmpeg.exe');
}

const getFFprobePath = () => {
  return (process.platform === 'linux') ?
      'ffprobe'
      :
      path.join(WIN_BINARIES_PATH, 'ffprobe.exe');
}

const handleWindows = () => {
  return new Promise(async (resolve, reject) => {
    if (!fs.existsSync(WIN_BINARIES_PATH)) {
        await createWindow();

        changeText(`Downloading FFmpeg ${WIN_FFMPEG_VERSION} ...`)
        try {
            const url = `https://www.gyan.dev/ffmpeg/builds/packages/ffmpeg-${WIN_FFMPEG_VERSION}-essentials_build.zip`;
            const res = await fetch(url);
            if (res.ok){
                const download_destination = path.resolve(os.tmpdir(), 'ffmpeg.zip');
                const extract_destination = path.resolve(os.tmpdir(), 'ffmpeg');
                const fileStream = fs.createWriteStream(download_destination, {flags: 'w'});
                await finished(Readable.fromWeb(res.body).pipe(fileStream));
                changeText(`Extracting archive ...`)
                const files = await decompress(download_destination, extract_destination);
                changeText(`Moving binaries to ${WIN_BINARIES_PATH} ...`)
                fs.cpSync(path.join(extract_destination, `ffmpeg-${WIN_FFMPEG_VERSION}-essentials_build`, 'bin'), WIN_BINARIES_PATH, {recursive: true});
                win.close()
                resolve();
            } else {
                changeText(`Could not download: ${url}`)
                reject()
            }
        } catch (e){
            win.setTitle('Error')
            changeText(e)
        }
    } else {
        console.log('FFmpeg already downloaded')
        resolve();
    }
  })
}

const handleLinux = () => {
  return new Promise((resolve, reject) => {
    exec('ffmpeg -version',
        async function (error, stdout, stderr) {
          if (stderr) {
            await createWindow();
            onError(`Please install FFmpeg and run the program again ! (${stderr})`);
          } else {
            exec('ffprobe -version',
                async function (error, stdout, stderr) {
                  if (stderr) {
                    await createWindow();
                    onError(`Please install FFprobe and run the program again ! (${stderr})`);
                  } else {
                    resolve()
                  }
                });
          }
        });
  });
}

const createWindow = async () => {
  win = new BrowserWindow({
    icon: 'icon.png',
    width: 500,
    height: 300,
    autoHideMenuBar: true,
    resizable: false,
    webPreferences: {
      preload: path.join(__dirname, 'script.js'),
      nodeIntegration: true,
      enableRemoteModule: true
    }
  })

  await win.loadFile(path.join(__dirname, 'index.html'))
}

const onError = (e) => {
    win.setTitle('Error')
    changeText(e)
}

const changeText = (text) => {
    console.log(text)
    win.webContents.send("changeText", text);
}


module.exports = {
  downloadFFmpeg,
  getFFmpegPath,
  getFFprobePath
};
