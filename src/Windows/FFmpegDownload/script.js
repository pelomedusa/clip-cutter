const {
    ipcRenderer
} = require("electron");


window.addEventListener('DOMContentLoaded', () => {
    const span = document.querySelector('span');
    ipcRenderer.on('changeText', (event, text) => {
        span.innerHTML = text;
    })
})
