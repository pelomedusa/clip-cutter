const { app, BrowserWindow, ipcMain, dialog} = require('electron/main')
const path = require('node:path')
const fs = require("fs");
const {generateFFmpegCommand} = require("./ffmpeg");
var exec = require('child_process').exec;
const treeKill = require('tree-kill');

let win = null;
let ffmpegProcessPid = null;

const createMainWindow = () => {
    win = new BrowserWindow({
        icon:'icon.png',
        width: 800,
        height: 1100,
        webPreferences: {
            preload: path.join(__dirname, 'script.js'),
            nodeIntegration: true,
            enableRemoteModule: true,
            contextIsolation: true,
            sandbox: true
        }
    })

    win.loadFile(path.join(__dirname, 'index.html'))

    win.on('closed', () => {
        if (ffmpegProcessPid !== null){
            console.log(`Killing FFmpeg process (${ffmpegProcessPid}) ...`)
            try {
                treeKill(ffmpegProcessPid)
            } catch (e){
                console.log(`Unable to kill process ${ffmpegProcessPid}: ${e}`)
            }
        }
        app.quit()
    })
}

const sendSubmitError = (text) => {
    win.webContents.send("submitError", text);
}

const validateData = (data) => {
    const {sourceVideo, clipName, start, end, encoding, mergeAudioTracks, outputPath, crf} = data;
    if (!fs.existsSync(sourceVideo)) {
        sendSubmitError(`Invalid video path:${sourceVideo}`);
        return false;
    }

    if (!clipName){
        sendSubmitError("Please specify a clip name");
        return false;
    }

    if (end < start) {
        sendSubmitError("Invalid start or end time");
        return false;
    }

    if (!encoding || !(['libx264', 'libx265'].includes(encoding))){
        sendSubmitError("Invalid encoding: " + encoding);
        return false;
    }

    if (!fs.existsSync(outputPath)){
        sendSubmitError(`Invalid output path:${outputPath}`);
        return false;
    }

    if (!crf || (crf < 0) || (crf > 51) ){
        sendSubmitError(`Invalid CRF: ${crf}`);
        return false;
    }

    return true;
}

ipcMain.on("submit", async (event, data) => {
    if (validateData(data)) {
        const {sourceVideo, clipName, start, end, encoding, mergeAudioTracks, outputPath, crf} = data;
        const command = await generateFFmpegCommand(sourceVideo, clipName, start, end, encoding, mergeAudioTracks, outputPath, crf)
        const childProcess = exec(command,
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                ffmpegProcessPid = null;
                if (error !== null) {
                    sendSubmitError(`Error: ${error}`);
                } else {
                    sendSubmitError(`Created clip`);
                }
            });
        ffmpegProcessPid = childProcess.pid;
    }
})

ipcMain.on('select-dirs', async (event, arg) => {
    const result = await dialog.showOpenDialog(win, {
        properties: ['openDirectory'],
    })
    if (result.filePaths.length){
        win.webContents.send("outputDirectorySelected", result.filePaths[0]);
    }
})


module.exports = {createMainWindow};
