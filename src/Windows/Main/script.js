const {
    ipcRenderer
} = require("electron");

let DOM;

window.addEventListener('DOMContentLoaded', () => {
    DOM = {
        fileinput: document.getElementById('fileinput'),
        dragndrop: document.getElementById('dragndrop'),
        controls: document.getElementById('controls'),
        videoPlayer: document.getElementById('videoPlayer'),
        clearVideo: document.getElementById('clearVideo'),
        totalDurationText: document.getElementById('totalDurationText'),
        cuttedDurationText: document.getElementById('cuttedDurationText'),
        inputStart: document.getElementById('inputStart'),
        inputEnd: document.getElementById('inputEnd'),
        startText: document.getElementById('startText'),
        endText: document.getElementById('endText'),
        submit: document.getElementById('submit'),
        loader: document.getElementById('loader'),
        clipName: document.getElementById('clipName'),
        muteButton: document.getElementById('muteButton'),
        videoEncoding: document.getElementById('videoEncoding'),
        mergeAudioTracks: document.getElementById('mergeAudioTracks'),
        outputDestination: document.getElementById('outputDestination'),
        crf: document.getElementById('crf'),
        crf_value: document.getElementById('crf_value'),
    }

    DOM.dragndrop.addEventListener('click', openVideoPicker)

    DOM.fileinput.addEventListener('change', onFileInputChange)

    DOM.videoPlayer.onloadedmetadata = onVideoLoaded;

    DOM.videoPlayer.addEventListener('timeupdate', onVideoTimerChange)

    DOM.clearVideo.addEventListener('click', () => {
        DOM.fileinput.value = null;
        DOM.videoPlayer.pause()
        onFileInputChange()
    })

    DOM.outputDestination.addEventListener('click', onOutputDirectoryClick)

    DOM.muteButton.addEventListener('click', toggleMuteSound)

    DOM.crf.addEventListener('change', onCrfChange)
    onCrfChange()

    DOM.submit.addEventListener('click', onSubmit)

    ipcRenderer.on('submitError', onSubmitError)
    ipcRenderer.on('outputDirectorySelected', onOutputDirectorySelected)

    restoreOutputPath()

})

const openVideoPicker = () => {
    DOM.fileinput.click()
}

const onFileInputChange = () => {
    if (DOM.fileinput.files.length){
        onFileSelect(DOM.fileinput.files[0])
    } else {
        DOM.controls.classList.add('hide');
        DOM.dragndrop.classList.remove('hide');
    }
}

const toggleMuteSound = () => {
    const muteButtonEmoji = DOM.muteButton.querySelector('span')
    if (DOM.videoPlayer.muted){
        DOM.videoPlayer.muted = false
        muteButtonEmoji.innerHTML = '🔊'
    } else {
        DOM.videoPlayer.muted = true
        muteButtonEmoji.innerHTML = '🔇'
    }
}
const onFileSelect = (file) => {
    DOM.dragndrop.classList.add('hide');
    DOM.controls.classList.remove('hide');
    DOM.videoPlayer.src = file.path
    DOM.videoPlayer.play()
}

const onVideoLoaded = () => {
    DOM.totalDurationText.innerHTML = `${DOM.videoPlayer.duration.toFixed(2)}s`;
}

let lastStartValue = null;
const onVideoTimerChange = () => {
    const duration = DOM.videoPlayer.duration;

    let start = parseFloat(DOM.inputStart.value);
    let end = parseFloat(DOM.inputEnd.value);

    if (lastStartValue !== start){
        DOM.videoPlayer.currentTime = start;
        lastStartValue = start;
    }


    if (start >= end){
        start = Math.max(0, end-1)
        DOM.inputStart.value = start;
        if (start <= 0 ){
            end = Math.min(100, start+1)
            DOM.inputEnd.value = end;
        }
    }

    const startInSeconds = ((start/100) * duration).toFixed(2);
    const endInSeconds = ((end/100) * duration).toFixed(2);

    if (
        (DOM.videoPlayer.currentTime > endInSeconds) ||
        (DOM.videoPlayer.currentTime < startInSeconds)
    ) {
        DOM.videoPlayer.currentTime = startInSeconds;
    }

    DOM.startText.innerHTML = `${startInSeconds}s`
    DOM.endText.innerHTML = `${endInSeconds}s`
    DOM.cuttedDurationText.innerHTML = `${(endInSeconds - startInSeconds).toFixed(2)}s`
}

const onSubmit = () => {

    setLoading(true);

    const duration = DOM.videoPlayer.duration;

    let start = parseFloat(DOM.inputStart.value);
    let end = parseFloat(DOM.inputEnd.value);

    const startInSeconds = ((start/100) * duration).toFixed(2);
    const endInSeconds = ((end/100) * duration).toFixed(2);

    ipcRenderer.send('submit', {
        sourceVideo: DOM.fileinput.files[0].path,
        clipName: DOM.clipName.value,
        start: startInSeconds,
        end: endInSeconds,
        encoding: DOM.videoEncoding.value,
        mergeAudioTracks: DOM.mergeAudioTracks.checked,
        outputPath: DOM.outputDestination.value,
        crf: DOM.crf.value
    });
}

const onSubmitError = (event, data) => {
    setLoading(false);
    alert(data);
}

const setLoading = (isLoading) => {
    if (isLoading){
        DOM.loader.classList.remove('hide')
        DOM.submit.classList.add('hide')
    } else {
        DOM.loader.classList.add('hide')
        DOM.submit.classList.remove('hide')
    }
}

const onOutputDirectoryClick = () => {
    ipcRenderer.send('select-dirs')
}
const onOutputDirectorySelected = (event, path) => {
    DOM.outputDestination.value = path
    if (path){
        localStorage.setItem('output_path', path)
    } else {
        localStorage.removeItem('output_path')
    }
}

const restoreOutputPath = () => {
    const path = localStorage.getItem('output_path');
    if (path) DOM.outputDestination.value = path;
}

const onCrfChange = () => {
    DOM.crf_value.innerHTML = DOM.crf.value;
}


