const util = require('util');
const exec = util.promisify(require('child_process').exec);
const path = require('node:path')

const {getFFmpegPath, getFFprobePath} = require("../FFmpegDownload");

const generateFFmpegCommand = async (sourceVideo, clipName, start, end, encoding, mergeAudioTracks, outputPath, crf) => {

  const ffmpegPath = getFFmpegPath(sourceVideo);

  const now = new Date();

  const filename = path.join(
      outputPath,
      clipName.replace(/[^a-z0-9]/gi, '_').toLowerCase() +
        '_' +
        now.getFullYear() +
        ('0' + now.getMonth()).slice(-2) +
        ('0' + now.getDay()).slice(-2) +
        '_' +
        ('0' + now.getHours()).slice(-2) +
        ('0' + now.getMinutes()).slice(-2) +
        ('0' + now.getSeconds()).slice(-2) +
        '.mkv'
  )

  let mergeAudioOption = '';
  let audiomap = '';
  if (mergeAudioTracks) {
    const ffProbePath = getFFprobePath();

    const countCommand = process.platform === "win32" ? 'find /v /c " "' : 'wc -w'

    const numberOfTracksCommand = `${ffProbePath} -v error -select_streams a -show_entries stream=index -of csv=p=0 "${sourceVideo}" | ${countCommand}`;
    const {stdout, stderr} = await exec(numberOfTracksCommand);
    if (stderr) return false;
    const numberOfTracks = parseInt(stdout.trim());
    mergeAudioOption = '-filter_complex "';
    for (i=0; i<numberOfTracks; i++){
      mergeAudioOption += `[0:a:${i}]`
    }

    mergeAudioOption += `amix=${numberOfTracks}:longest[aout]"`
    audiomap = ' -map 0:V:0 -map "[aout]"'
  }

  return `${ffmpegPath} -ss ${start} -i "${sourceVideo}"  -t ${end-start} ${mergeAudioOption} ${audiomap} -c:v ${encoding} -${encoding.replace('lib', '')}-params crf=${crf} -c:a aac -b:a 320k -progress pipe:1 "${filename}"`
}

module.exports = {
  generateFFmpegCommand
};
